package com.src.listeners;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.src.arena.Arena;

public class Checkpoints implements Listener {

	public static HashMap<Player, Location> checkpoint = new HashMap<Player, Location>();
	public static HashMap<Player, Arena> arena = new HashMap<Player, Arena>();

	@EventHandler
	public void pressurePlates(PlayerInteractEvent e) {

		if (e.getAction() == Action.PHYSICAL) {

			if (e.getClickedBlock().getType().equals(Material.IRON_PLATE)) {

				checkpoint.put(e.getPlayer(), e.getClickedBlock().getLocation()
						.add(0, 1, 0));

			}

			if (e.getClickedBlock().getType().equals(Material.WOOD_PLATE)) {

				Arena a = arena.get(e.getPlayer());

				if(a != null){
					a.Stop(e.getPlayer());
				}else{
					e.getPlayer().sendMessage("You Broke it");
				}
			}

		}

	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {

		if(arena == null || !arena.containsKey(e.getPlayer())) return;
		
		Arena a = arena.get(e.getPlayer());
		
		if(a.getCanMove() == false){
			e.setTo(e.getFrom());
		}
		
		if (e.getPlayer().getLocation().getBlock().getType()
				.equals(Material.STATIONARY_WATER)) {
			e.getPlayer().teleport(checkpoint.get(e.getPlayer()));
			return;
		}
		
		

	}

}
