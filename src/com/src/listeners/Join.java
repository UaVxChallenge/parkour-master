package com.src.listeners;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.src.Utils.CoinManager;
import com.src.Utils.LocationManager;
import com.src.arena.Arena;
import com.src.arena.ArenaManager;
import com.src.parkour.Main;

public class Join implements Listener {

	public static ArrayList<Player> players = new ArrayList<Player>();

	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent e) {
		
		e.setJoinMessage("");

		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {
			
			@Override
			public void run() {
				
				e.getPlayer().teleport(LocationManager.Lobby);
				
			}
		}, 5);
		CoinManager.getCoins(e.getPlayer());
		if(players.size() == 0){
			players.add(e.getPlayer());
			return;
		}

		if (players.size() == 1) {
			players.add(e.getPlayer());
			ArenaManager.am.createArena(players);
			players.clear();
		}
		

	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		
		if(players.contains(e.getPlayer())){
			players.remove(e.getPlayer());
		}

		Arena a = null;
		if(Checkpoints.arena.containsKey(e.getPlayer())){
			
			a = Checkpoints.arena.get(e.getPlayer());
			
		}

		if (a == null) {
			return;
		}
		if (a.getPosition(e.getPlayer()).equalsIgnoreCase("player1")) {
			a.Stop(a.getPlayer2());
		} else {
			a.Stop(a.getPlayer1());
		}
		a.clear();

	}

}
