package com.src.arena;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.src.Utils.LocationManager;
import com.src.listeners.Checkpoints;
import com.src.parkour.Main;

public class Arena {

	private int id = 0;
	private List<UUID> players = new ArrayList<UUID>();
	public static HashMap<Integer, ArrayList<Player>> ArenaGame = new HashMap<Integer, ArrayList<Player>>();
	private Player Player1 = null;
	private Player Player2 = null;
	private boolean Started = false;
	private int gameID = 0;
	private boolean canMove = true;
	public static HashMap<Player, Long> Timer = new HashMap<Player, Long>();

	public Arena(int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public List<UUID> getPlayers() {
		return players;
	}

	public void addPlayers(int i, ArrayList<Player> players) {

		ArenaGame.put(i, players);
		
		int j = 1;
		
		for(Player p : players){
			
			if(j == 1){
				Player1 = p;
			}
			if(j == 2){
				Player2 = p;
			}
			j++;
		}
	}

	public void doInvis(Player p) {

		for (Player pl : Bukkit.getOnlinePlayers()) {

			pl.hidePlayer(p);
			p.hidePlayer(pl);

		}

	}

	public void Start() {
		Bukkit.getServer().getScheduler().cancelTask(gameID);
		
		canMove = true;

		for (UUID id : getPlayers()) {
			Player p = Bukkit.getPlayer(id);

			p.sendMessage(ChatColor.BLUE + "[Parkour Master] " + ChatColor.GOLD
					+ "Race has started");
		}
		
		Timer.put(Player1, System.currentTimeMillis());
		Timer.put(Player2, System.currentTimeMillis());
	}

	public void Stop(Player Winner) {

		Player loser = null;
		
		if(Winner == Player1){
			loser = Player2;
		}else{
			loser = Player1;
		}
		
		ArenaManager.am.GameOver(Winner, loser, getId());

		End();
	}

	public boolean isStarted() {
		return Started;
	}

	public void End() {
		Started = false;
	}

	public void StartGame() {
		
		Checkpoints.arena.put(Player1, this);
		Checkpoints.arena.put(Player2, this);

		gameID = Bukkit.getServer().getScheduler()
				.scheduleSyncRepeatingTask(Main.instance, new Runnable() {

					int GameTimer = 10;

					public void run() {

						if (GameTimer > 0) {
							for (UUID id : getPlayers()) {
								Player p = Bukkit.getPlayer(id);
								p.sendMessage(ChatColor.BLUE
										+ "[Parkour Master] " + ChatColor.GOLD
										+ "Game starts in " + GameTimer
										+ " seconds!");
							}

						}
						if(GameTimer == 5){
							canMove = false;
							doInvis(Player1);
							doInvis(Player2);
							Player1.teleport(LocationManager.SpawnPlayer1);
							Checkpoints.checkpoint.put(Player1, LocationManager.SpawnPlayer1);
							Player2.teleport(LocationManager.SpawnPlayer2);
							Checkpoints.checkpoint.put(Player2, LocationManager.SpawnPlayer2);

						}
						
						if(GameTimer == 3){
							for (UUID id : getPlayers()) {
								Player p = Bukkit.getPlayer(id);
								LocationManager.t3.send(p);
							}
						}
						if(GameTimer == 2){
							for (UUID id : getPlayers()) {
								Player p = Bukkit.getPlayer(id);
								LocationManager.t2.send(p);
							}
						}
						if(GameTimer == 1){
							for (UUID id : getPlayers()) {
								Player p = Bukkit.getPlayer(id);
								LocationManager.t1.send(p);
							}
						}
						if (GameTimer == 0) {
							Player1.showPlayer(Player2);
							Player2.showPlayer(Player1);
							Start();
						}
						
						GameTimer--;
					}
				}, 20, 20);

	}

	public String getPosition(Player p) {
		if (p == Player1) {
			return "Player1";
		} else {
			return "Player2";
		}
	}
	
	public Player getPlayer1(){
		return Player1;
	}
	
	public Player getPlayer2(){
		return Player2;
	}
	
	public boolean getCanMove(){
		return canMove;
	}
	
	public void clear(){
		players.clear();
	}

}