package com.src.arena;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.src.Utils.CoinManager;
import com.src.parkour.Main;

public class ArenaManager {

	public Map<String, Location> locs = new HashMap<String, Location>();
	public static ArenaManager am = new ArenaManager();
	public static List<Arena> arenas = new ArrayList<Arena>();
	public static int arenaSize = 0;
	public static HashMap<UUID, Integer> PlayerArena = new HashMap<UUID, Integer>();

	public ArenaManager() {

	}

	public static ArenaManager getManager() {
		return am;
	}

	public Arena getArena(int i) {
		for (Arena a : arenas) {
			if (a.getId() == i) {
				return a;
			}
		}
		return null;
	}

	public Arena createArena(ArrayList<Player> players) {
		int num = arenaSize;
		arenaSize++;

		Arena a = new Arena(num);
		arenas.add(a);
		
		for (Player p : players) {
			a.addPlayers(num, players);
			p.sendMessage(ChatColor.GREEN + "Arena " + num
					+ " has been created for you!");
			a.doInvis(p);
		}

		a.StartGame();

		return a;
	}

	public void removeArena(int i) {
		Arena a = getArena(i);
		if (a == null) {
			return;
		}
		arenas.remove(a);

	}

	public void GameOver(final Player winner, final Player loser, int arena) {

		long l = 0;
		long hours = 0;
		long minutes = 0;
		long seconds = 0;

		if (Arena.Timer.containsKey(winner)) {
			l = Arena.Timer.get(winner);
			
			l = -1 * (l - System.currentTimeMillis());

			seconds = TimeUnit.MILLISECONDS.toSeconds(l);
			
			if(seconds >= 3600){
				hours = seconds / 3600;
				seconds = seconds % 3600;
			}
			if(3600 > seconds && seconds > 60){
				minutes = seconds / 60;
				seconds = seconds % 60;
			}
			
			Arena.Timer.remove(winner);

		}

		if (winner.isOnline()) {
			winner.sendMessage(ChatColor.BLUE + "[Parkour Master] "
					+ ChatColor.GOLD + winner.getName()
					+ " has won the game! (+20 Coins)");
			winner.sendMessage(ChatColor.BLUE + "[Parkour Master]"
					+ ChatColor.GOLD + winner.getName()
					+ " finished the game in " + hours + ":" + minutes + ":"
					+ seconds);
		}
		if (loser.isOnline()) {
			loser.sendMessage(ChatColor.BLUE + "[Parkour Master] "
					+ ChatColor.GOLD + winner.getName() + " has won the game!");
			loser.sendMessage(ChatColor.BLUE + "[Parkour Master]"
					+ ChatColor.GOLD + winner.getName()
					+ " finished the game in " + hours + ":" + minutes + ":"
					+ seconds);
		}

		CoinManager.addCoins(winner, 20);

		Bukkit.getServer().getScheduler()
				.scheduleSyncDelayedTask(Main.instance, new Runnable() {

					@Override
					public void run() {

						ByteArrayDataOutput out = ByteStreams.newDataOutput();

						out.writeUTF("Connect");
						out.writeUTF("Hub");

						if (winner.isOnline()) {
							winner.sendPluginMessage(Main.instance,
									"BungeeCord", out.toByteArray());
						}
						if (loser.isOnline()) {
							loser.sendPluginMessage(Main.instance,
									"BungeeCord", out.toByteArray());
						}
					}
				}, 20);

		removeArena(arena);
		PlayerArena.remove(winner.getUniqueId());
		PlayerArena.remove(loser.getUniqueId());
	}
}
