package com.src.Utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class CoinManager {
	
	public static Connection connection;
	public static HashMap<UUID, Integer> coins = new HashMap<UUID, Integer>();

	public static void addCoins(Player p, int amount){
		
		int temp = coins.get(p.getUniqueId());
		temp = temp + amount;
		
		try {
			if (playerDataContainsPlayer(p)) {

				openConnection();

				PreparedStatement sql = connection
						.prepareStatement("UPDATE `player_data` SET coins=? WHERE uuid=?;");
				sql.setInt(1, temp);
				sql.setString(2, "" + p.getUniqueId());
				sql.execute();
				sql.close();
			} else {
					openConnection();
				
				PreparedStatement newPlayer = connection
						.prepareStatement("INSERT INTO `player_data` values(\""
								+ p.getUniqueId() + "\", " + temp + ")");
				newPlayer.execute();
				newPlayer.close();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			closeConnection();
		}
		
	}
	
	public synchronized static void openConnection(){
		try{
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/Minigame", "root", "Ne@t-815");
			
			Bukkit.getLogger().info("Connection opened");
		}catch(Exception ex){
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public synchronized static void closeConnection(){
		try{
			if(connection != null){
				connection.close();	
				Bukkit.getLogger().info("Connection closed");
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public synchronized static boolean playerDataContainsPlayer(Player player){

		try{
			openConnection();
			PreparedStatement sql = connection.prepareStatement("SELECT * FROM `player_data` WHERE uuid=?;");
			sql.setString(1, "" + player.getUniqueId());
			ResultSet resultset = sql.executeQuery();
			boolean containsPlayer = resultset.next();
			
			sql.close();
			resultset.close();
			
			return containsPlayer;
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			closeConnection();
		}
		
	}
	
	public static void getCoins(Player p){		
		try{
			if(playerDataContainsPlayer(p)){
				
				openConnection();
				
				PreparedStatement sql = connection.prepareStatement("SELECT coins FROM `player_data` WHERE uuid=?;");
				sql.setString(1, "" + p.getUniqueId());
				
				ResultSet result = sql.executeQuery();
				result.next();
				
				coins.put(p.getUniqueId(), result.getInt("coins"));
				
				sql.close();
				result.close();

			}else{
				
				coins.put(p.getUniqueId(), 0);
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			closeConnection();
		}
		
	}
}
