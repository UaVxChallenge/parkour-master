package com.src.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;

public class LocationManager {

	public static Location SpawnPlayer1 = new Location(Bukkit.getWorld("world"), 443, 64, -231);
	public static Location SpawnPlayer2 = new Location(Bukkit.getWorld("world"), 444, 64, -227);
	public static Location Lobby = new Location(Bukkit.getWorld("world"), 505, 88, -195);
	public static Title t3;
	public static Title t2;
	public static Title t1;

	public static void setupTitles(){
		
		t3 = new Title("3", "Game starts in :", 5, 10, 5);
		t3.setTitleColor(ChatColor.GREEN);
		t3.setSubtitleColor(ChatColor.GOLD);
		
		t2 = new Title("2", "Game starts in :", 5, 10, 5);
		t2.setTitleColor(ChatColor.YELLOW);
		t2.setSubtitleColor(ChatColor.GOLD);
		
		t1 = new Title("1", "Game starts in :", 5, 10, 5);
		t1.setTitleColor(ChatColor.RED);
		t1.setSubtitleColor(ChatColor.GOLD);
	}
}
