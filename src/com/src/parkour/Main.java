package com.src.parkour;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.src.Utils.LocationManager;
import com.src.arena.Arena;
import com.src.listeners.Checkpoints;
import com.src.listeners.Join;

public class Main extends JavaPlugin{
	
	public static Plugin instance = null;

	public void onEnable(){
		
		instance = this;
		
		LocationManager.setupTitles();
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

		
		Bukkit.getServer().getPluginManager().registerEvents(new Join(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Checkpoints(), this);
	}
	
	public void onDisable(){
		
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		if(!(sender instanceof Player)){
			return false;
		}
		Player p = (Player) sender;
		
		if(cmd.getName().equalsIgnoreCase("Hub")){
			
			Arena a = null;
			if(Checkpoints.arena.containsKey(p)){
				
				a = Checkpoints.arena.get(p);
			}
			
			if(a != null){
				for(UUID id : a.getPlayers()){
					Player Winner = null;
					if(Bukkit.getPlayer(id) != p){
						Winner = Bukkit.getPlayer(id);
						a.Stop(Winner);	
					}
					
				}
			}
			
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			
			out.writeUTF("Connect");
			out.writeUTF("Hub");
			
			p.sendPluginMessage(Main.instance, "BungeeCord", out.toByteArray());
			return true;
		}
		return false;
	}
	
}
